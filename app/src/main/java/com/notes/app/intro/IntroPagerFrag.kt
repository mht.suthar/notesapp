package com.notes.app.intro

import android.content.Intent
import com.notes.app.R
import com.notes.app.base.BaseFrag
import com.notes.app.dashboard.DashboardActivity
import com.notes.app.databinding.FragIntoPagerBinding

class IntroPagerFrag(
    var message: String,
    var introIcon: Int,
    var hasLastPage: Boolean = false,
    var pagerChange: (() -> Unit)
) : BaseFrag<FragIntoPagerBinding>(R.layout.frag_into_pager) {

    override fun init() {
        binding.txtMessage.text = message
        binding.imgIntro.setImageResource(introIcon)
        setClickListener()
    }

    private fun setClickListener() {
        binding.cardNext.setOnClickListener {
            if (hasLastPage)
                moveToDashboard()
            else
                pagerChange()
        }
    }

    private fun moveToDashboard() {
        Intent(requireContext(), DashboardActivity::class.java).apply {
            startActivity(this)
            requireActivity().finish()
        }
    }
}