package com.notes.app.intro

import androidx.fragment.app.Fragment
import com.notes.app.R
import com.notes.app.base.BaseAct
import com.notes.app.databinding.ActIntroBinding
import com.notes.app.utils.CommonPagerAdapter
import java.util.ArrayList

class IntroAct : BaseAct<ActIntroBinding>(R.layout.act_intro) {

    override fun init() {
        setPagerAdapter()
    }

    private fun setPagerAdapter() {
        binding.pager.adapter = CommonPagerAdapter(
            this, getListOfFrag()
        )
    }

    private fun getListOfFrag(): ArrayList<Fragment> {
        return arrayListOf(
            IntroPagerFrag(getString(R.string.add_task_to__), R.drawable.ic_add, false, ::changePager),
            IntroPagerFrag(getString(R.string.long_press_the__), R.drawable.ic_add, false, ::changePager),
            IntroPagerFrag(getString(R.string.let_give_you), R.drawable.ic_add,false, ::changePager),
            IntroPagerFrag(getString(R.string.customize_your_own_theme), R.drawable.ic_add, true,::changePager),
        )
    }

    private fun changePager(){
        binding.pager.currentItem = binding.pager.currentItem + 1
    }
}