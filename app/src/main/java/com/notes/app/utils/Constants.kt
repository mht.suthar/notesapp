package com.notes.app.utils

object Constants {

    const val TIME_STAMP_FORMAT = "yyyy-MM-dd HH:MM:SS.SSS"
}