package com.notes.app.utils

import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import android.os.Build
import androidx.recyclerview.widget.RecyclerView
import com.notes.app.R

@BindingAdapter("modelName")
fun setTextWithModel(textView: AppCompatTextView, url: String?) {
    val manufacturer = Build.MANUFACTURER
    val model = Build.MODEL
    if (model.startsWith(manufacturer)) {
        textView.text = capitalize(model)
    } else {
        textView.text = capitalize(manufacturer).toString() + " " + model
    }
}

@BindingAdapter("getDay")
fun setDay(textView: AppCompatTextView, date: String?) {
    textView.text = date?.dateFormat(Constants.TIME_STAMP_FORMAT, "dd")
}


@BindingAdapter("setItemDecor")
fun setItemDecor(rv: RecyclerView, enable: Boolean) {
    rv.addItemDecoration(
        ItemDecorationColumns(
            rv.context.resources.getDimensionPixelSize(R.dimen.note_list_spacing),
            2
        )
    )
}

@BindingAdapter("getWeekDay")
fun setWeekDay(textView: AppCompatTextView, date: String?) {
    textView.text = date?.dateFormat(Constants.TIME_STAMP_FORMAT, "EEE")
}

@BindingAdapter("getMonth")
fun setMonth(textView: AppCompatTextView, date: String?) {
    textView.text = date?.dateFormat(Constants.TIME_STAMP_FORMAT, "MMM")
}

private fun capitalize(s: String?): String? {
    if (s == null || s.isEmpty()) {
        return ""
    }
    val first = s[0]
    return if (Character.isUpperCase(first)) {
        s
    } else {
        Character.toUpperCase(first).toString() + s.substring(1)
    }
}