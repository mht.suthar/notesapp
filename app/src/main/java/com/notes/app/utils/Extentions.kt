package com.notes.app.utils

import com.notes.app.utils.Constants.TIME_STAMP_FORMAT
import java.text.SimpleDateFormat
import java.util.*

fun getCurrentDate() : String{
    val c: Date = Calendar.getInstance().time
    val df = SimpleDateFormat(TIME_STAMP_FORMAT, Locale.getDefault())
    return df.format(c)
}

fun String.dateFormat(inputFormat: String, outPutFormat: String): String {
    return SimpleDateFormat(outPutFormat, Locale.getDefault())
        .format(
            SimpleDateFormat(inputFormat, Locale.getDefault())
                .parse(this)
        )
}