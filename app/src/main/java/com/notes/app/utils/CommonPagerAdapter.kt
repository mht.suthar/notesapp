package com.notes.app.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class CommonPagerAdapter : FragmentStateAdapter {

    private val listOfFrag: java.util.ArrayList<Fragment>

    constructor(fa: FragmentActivity, listOfFrag: ArrayList<Fragment>) : super(fa) {
        this.listOfFrag = listOfFrag
    }

    constructor(frg: Fragment, listOfFrag: ArrayList<Fragment>) : super(frg) {
        this.listOfFrag = listOfFrag
    }

    override fun getItemCount(): Int = listOfFrag.size

    override fun createFragment(position: Int): Fragment = listOfFrag[position]
}