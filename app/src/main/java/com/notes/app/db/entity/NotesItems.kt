package com.notes.app.db.entity;

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.notes.app.db.utils.TimestampConverter
import java.util.*

@Entity(tableName = "notes")
data class NotesItems(
    @PrimaryKey(autoGenerate = true)
    var id: Int?,

    @ColumnInfo(name = "message")
    var message: String,

    @ColumnInfo(name = "isNotificationEnable")
    var isNotificationEnable: Boolean,

    @ColumnInfo(name = "isCompleted")
    var isCompleted: Boolean,

    @ColumnInfo(name = "date")
    var createDate: String,

    /*@TypeConverters(TimestampConverter::class)
    var createdfDate : Date?*/
)
