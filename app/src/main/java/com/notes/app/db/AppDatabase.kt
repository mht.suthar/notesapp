package com.notes.app.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.notes.app.db.dio.NotesDao
import com.notes.app.db.entity.NotesItems

@Database(entities = [NotesItems::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun notesDao(): NotesDao
}