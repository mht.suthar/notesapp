package com.notes.app.db.dio

import androidx.room.*
import com.notes.app.db.entity.NotesItems

@Dao
interface NotesDao {
    @Query("SELECT * FROM notes")
    suspend fun getAll(): List<NotesItems>

    @Insert
    suspend fun insertAll(vararg notesEntity: NotesItems)

    @Insert
    suspend fun insert(notesEntity: NotesItems)

    @Delete
    suspend fun delete(notesEntity: NotesItems)

    @Update
    suspend fun update(vararg notesEntity: NotesItems)
}