package com.notes.app.db.utils

import androidx.room.TypeConverter
import com.notes.app.utils.Constants
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class TimestampConverter {
    var df: DateFormat = SimpleDateFormat(Constants.TIME_STAMP_FORMAT)
    @TypeConverter
    fun fromTimestamp(value: String?): Date? {
        return if (value != null) {
            try {
                return df.parse(value)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            null
        } else {
            null
        }
    }
}