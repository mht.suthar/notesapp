package com.notes.app.dashboard.model

import androidx.lifecycle.ViewModel
import com.notes.app.db.AppDatabase
import com.notes.app.db.entity.NotesItems
import com.notes.app.utils.getCurrentDate
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DashboardActVM @Inject constructor(private val db: AppDatabase) : ViewModel() {

    suspend fun addNotes(message : String, isNotificationEnable : Boolean) {
        db.notesDao().insert(NotesItems(null, message =  ""+message, isNotificationEnable = isNotificationEnable, createDate = ""+ getCurrentDate(), isCompleted = false))
    }

    suspend fun getAllNotes(): List<NotesItems>{
        return db.notesDao().getAll()
    }

    suspend fun updateNote(notes: NotesItems, check: Boolean) {
        notes.isCompleted = check
        db.notesDao().update(notes)
    }
}