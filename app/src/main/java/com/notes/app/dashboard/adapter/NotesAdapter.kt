package com.notes.app.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleObserver
import androidx.recyclerview.widget.RecyclerView
import com.notes.app.R
import com.notes.app.databinding.ItemNoteBinding
import com.notes.app.db.entity.NotesItems

class NotesAdapter(
    private var notesList: List<NotesItems>,
    private var context: Context,
    private var clickHandle: ((banner: NotesItems, isCheck: Boolean) -> Unit)? = null
) : RecyclerView.Adapter<NotesAdapter.ViewHolder>(), LifecycleObserver {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemNoteBinding>(
            LayoutInflater.from(context),
            R.layout.item_note,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = notesList.size

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.binding.note = notesList[pos]
    }

    inner class ViewHolder(val binding: ItemNoteBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.check.setOnCheckedChangeListener { _, isChecked ->
                clickHandle?.let { it(notesList[adapterPosition], isChecked) }
            }
            binding.root.setOnLongClickListener {

                true
            }
        }
    }
}