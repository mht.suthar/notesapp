package com.notes.app.dashboard

import android.annotation.SuppressLint
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.notes.app.R
import com.notes.app.base.BaseAct
import com.notes.app.dashboard.adapter.NotesAdapter
import com.notes.app.dashboard.model.DashboardActVM
import com.notes.app.databinding.ActDashboardBinding
import com.notes.app.databinding.DialogAddNoteBinding
import com.notes.app.db.entity.NotesItems
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@AndroidEntryPoint
class DashboardActivity : BaseAct<ActDashboardBinding>(R.layout.act_dashboard) {

    private val viewModel: DashboardActVM by viewModels()
    private lateinit var mAdapter: NotesAdapter

    override fun init() {
        getNotesList()
        setClickListeners()
    }

    private fun getNotesList() {
        CoroutineScope(Dispatchers.Main).launch {
            setNotesAdapter(viewModel.getAllNotes())
        }
    }

    private fun setClickListeners() {
        binding.toolbar.imgAdd.setOnClickListener {
            showBottomSheetDialog()
        }
    }

    private fun setNotesAdapter(allNotesList: List<NotesItems>) {
        setLinProgress(allNotesList)
        mAdapter = NotesAdapter(allNotesList, this) { notes, isCheck ->
            updateNote(notes, isCheck)
        }
        binding.rvNotes.adapter = mAdapter
    }

    private fun updateNote(notes: NotesItems, check: Boolean) {
        CoroutineScope(Dispatchers.Main).launch {
            viewModel.updateNote(notes, check)
            //TODO Need to improve this logic
            //getNotesList()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setLinProgress(allNotesList: List<NotesItems>) {
        if(allNotesList.isEmpty())
            return
        val progressCount = allNotesList.count { it.isCompleted }
        binding.progress.max = allNotesList.size
        binding.progress.progress = progressCount
        binding.txtProgress.text =  "${(progressCount.toDouble() / allNotesList.size.toDouble()) * 100} %"
    }

    private fun showBottomSheetDialog() {
        val bottomSheetDialog = BottomSheetDialog(this)
        val bindingSheet = DataBindingUtil.inflate<DialogAddNoteBinding>(
            layoutInflater,
            R.layout.dialog_add_note,
            null,
            false
        )
        bottomSheetDialog.setContentView(bindingSheet.root)
        bindingSheet.btnSubmit.setOnClickListener {
            if(bindingSheet.edtMessage.text.isNullOrEmpty()){
                showMessage(getString(R.string.enter_message))
                return@setOnClickListener
            }
            bottomSheetDialog.hide()
            addNotes(bindingSheet.edtMessage.text?.trim().toString(), bindingSheet.switchNotification.isChecked)
        }
        bottomSheetDialog.show()
    }

    private fun addNotes(message: String, isNotification : Boolean) {
        CoroutineScope(Dispatchers.Main).launch {
            viewModel.addNotes(message, isNotification)
            //TODO Need to improve this logic
            getNotesList()
        }
    }
}